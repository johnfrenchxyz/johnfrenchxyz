// Required Modules
const babel 			= require('gulp-babel');
const cleanCSS		= require('gulp-clean-css');
const gulp				= require('gulp');
const minify			= require('gulp-minify');
const nunjucks		= require('gulp-nunjucks');
const pump				= require('pump');
const rename			= require('gulp-rename');
const sass				= require('gulp-sass');
const sourcemaps	= require('gulp-sourcemaps');

// HTML Task
gulp.task('html', () => {
	pump([
		gulp.src('views/*.njk'),
		nunjucks.compile(),
		rename((path) => {
			path.extname = '.html';
		}),
		gulp.dest('public')
	]);
});

// CSS Task
gulp.task('css', () => {
	pump([
		gulp.src(['scss/app.scss']),
		sourcemaps.init(),
		sass().on('error', sass.logError),
		cleanCSS(),
		sourcemaps.write('.'),
		gulp.dest('public/resources/css')
	]);
});

// JavaScript Task
gulp.task('js', () => {
	pump([
		gulp.src([
			'scripts/**/*.js'
		]),
		sourcemaps.init(),
		babel(),
		minify({
			ext: {
				min: '.js'
			},
			noSource: true,
			preserveComments: 'some'
		}),
		sourcemaps.write('.'),
		gulp.dest('public/resources/js')
	]);
});

// Watch Task
gulp.task('watch', () => {
	gulp.watch(['views/**/*.njk'], ['html']);
	gulp.watch(['scss/**/*.scss'], ['css']);
	gulp.watch(['scripts/**/*.js'], ['js']);
});

// Default Task
gulp.task('default', ['html', 'css', 'js', 'watch']);

// All Task
gulp.task('all', ['html', 'css', 'js']);
