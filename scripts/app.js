// Mobile Nav
// ----------

// Define the mobile nav open button
const mobileNavButton = document.querySelector('.mobile-nav-button');
// Define the mobile menu
const nav = document.querySelector('nav');
// Define the mobile nav close button
const closeNav = document.querySelector('.close-nav');

mobileNavButton.addEventListener('click', () => {
	nav.classList.add('mobile-nav-active');
	closeNav.addEventListener('click', () => {
		nav.classList.remove('mobile-nav-active');
	});
	nav.addEventListener('click', () => {
		nav.classList.remove('mobile-nav-active');
	});
});

// Shelf Gallery
$('#shelfGallery').on('click', function() {
  $(this).lightGallery({
    dynamic: true,
    dynamicEl: [{
      "src": 'resources/images/shelf-preview/shelf-1.png'
    }, {
      "src": 'resources/images/shelf-preview/shelf-2.png'
    }, {
      "src": 'resources/images/shelf-preview/shelf-3.png'
    }, {
      "src": 'resources/images/shelf-preview/shelf-4.png'
    }, {
      "src": 'resources/images/shelf-preview/shelf-5.png'
    }, {
      "src": 'resources/images/shelf-preview/shelf-6.png'
    }]
  })
});
